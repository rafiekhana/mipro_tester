import './Header.css'
import { Typography, Grid, Link} from '@mui/material';

function Header () {

    return (
        <Grid
            container
            direction="row"
            justifyContent="space-between"
            alignItems="center"
            sx={{ backgroundColor: "#214457", padding: "28px 90px"}}>
                <Grid
                    item
                    alignItems="center"
                    sx={{ display: "flex", flexDirection: "row"}}>
                        <img src="Pictures/SeeEventLogo(3).svg" alt="Logo" />
                        <Typography
                            sx={{
                                fontFamily: "Helvetica, sans-serif",
                                color: "white",
                                lineHeight: '28px',
                                fontStyle: "italic",
                                fontWeight: "Bold",
                                fontSize: '28px',
                                marginLeft: "10px",
                            }}>SeeEvent
                        </Typography>
                </Grid>
                <Grid
                    item
                    direction="row"
                    alignItems="center">
                        <Link sx={{ marginRight: "24px", fontFamily: "'Noto Sans', sans-serif", fontSize: '18px', color: "#FFFFFF" }} href="#" underline="none">
                            {'Sign Up'}
                        </Link>
                        <Link sx={{ fontFamily: "'Noto Sans', sans-serif", fontSize: '18px', color: "#FFFFFF" }}href="#" underline="none">
                            {'Sign In'}
                        </Link>
                </Grid>
        </Grid>
    )
}

export default Header;