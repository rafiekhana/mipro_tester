import './Footer.css'
import { Typography, Grid, Link, Box} from '@mui/material';
// import FacebookRounded from '@mui/icons-material/FacebookRounded';

function Footer() {

    return (
        <Grid
            container
            direction="column"
            justifyContent="center"
            alignItems="center"
            sx={{ backgroundColor: "#214457", padding: "48px 96px", color:"white"}}>
            <Grid
                container
                direction="row"
                sx={{ marginBottom:"48px" }}>
                    <Grid
                        item
                        xs={5}
                        alignItems="flex-start"
                        sx={{ display: "flex", flexDirection: "column", paddingRight: "42px"}}>

                            <Grid
                                item
                                alignItems="center"
                                sx={{ display: "flex", flexDirection: "row", marginBottom: "16px"}}>
                                    <img src="Pictures/SeeEventLogo(4).svg" alt="Logo" />
                                    <Typography
                                        sx={{
                                            fontFamily: "Helvetica, sans-serif",
                                            color: "#F2D555",
                                            lineHeight: '28px',
                                            fontStyle: "italic",
                                            fontWeight: "Bold",
                                            fontSize: '28px',
                                            marginLeft: "10px",
                                        }}>SeeEvent
                                    </Typography>
                            </Grid>

                            <Typography
                                sx={{
                                    fontFamily: "'Noto Sans', sans-serif",
                                    lineHeight: '22.4px',
                                    fontWeight: "400",
                                    fontSize: '16px',
                                    marginBottom: "32px",
                                }}>SeeEvent is a platform where you can create or find an amazing events around the world.
                            </Typography>

                            <Grid>
                                <Typography
                                    sx={{
                                        fontFamily: "'Noto Sans', sans-serif",
                                        lineHeight: '25.2px',
                                        fontWeight: "Bold",
                                        fontSize: '16px',
                                        marginBottom: "8px",
                                    }}>Follow Us on
                                </Typography>
                                <Box>
                                    <img style={{paddingRight: "14px"}} src="Pictures/Facebook.svg" />
                                    <img style={{paddingRight: "14px"}} src="Pictures/Instagram.svg" />
                                    <img style={{paddingRight: "14px"}} src="Pictures/Twitter.svg" />
                                    <img src="Pictures/Youtube.svg" />
                                </Box>
                            </Grid>

                            
                    </Grid>
                    <Grid
                        item
                        xs={2}>
                            <Typography
                                sx={{
                                    fontFamily: "'Noto Sans', sans-serif",
                                    fontWeight: "Bold",
                                    fontSize: '16px',
                                    marginBottom: "14px"
                                }}>Links
                            </Typography>
                            <Typography
                                sx={{
                                    fontFamily: "'Noto Sans', sans-serif",
                                    fontWeight: "Normal",
                                    fontSize: '14px',
                                    display: "flex",
                                    flexDirection: "column"
                                }}>
                                    <Link sx={{ paddingBottom: "8px", fontFamily: "'Noto Sans', sans-serif", fontSize: '14px', color: "#FFFFFF", '&:hover': {color:"#F2D555"} }} href="#" underline="none">
                                        {'Home'}
                                    </Link>
                                    <Link sx={{ paddingBottom: "8px", fontFamily: "'Noto Sans', sans-serif", fontSize: '14px', color: "#FFFFFF", '&:hover': {color:"#F2D555"} }} href="#" underline="none">
                                        {'Explore'}
                                    </Link>
                                    <Link sx={{ paddingBottom: "8px", fontFamily: "'Noto Sans', sans-serif", fontSize: '14px', color: "#FFFFFF", '&:hover': {color:"#F2D555"} }} href="#" underline="none">
                                        {'My Events'}
                                    </Link>
                                    <Link sx={{ fontFamily: "'Noto Sans', sans-serif", fontSize: '14px', color: "#FFFFFF", '&:hover': {color:"#F2D555"} }} href="#" underline="none">
                                        {'Bookmarks'}
                                    </Link>
                            </Typography>
                    </Grid>
                    <Grid
                        item
                        xs={2}>
                            <Typography
                                sx={{
                                    fontFamily: "'Noto Sans', sans-serif",
                                    fontWeight: "Bold",
                                    fontSize: '16px',
                                    marginBottom: "14px"
                                }}>Top Categories
                            </Typography>
                            <Typography
                                sx={{
                                    fontFamily: "'Noto Sans', sans-serif",
                                    fontWeight: "Normal",
                                    fontSize: '14px',
                                    display: "flex",
                                    flexDirection: "column"
                                }}>
                                    <Link sx={{ paddingBottom: "8px", fontFamily: "'Noto Sans', sans-serif", fontSize: '14px', color: "#FFFFFF", '&:hover': {color:"#F2D555"} }} href="#" underline="none">
                                        {'Design'}
                                    </Link>
                                    <Link sx={{ paddingBottom: "8px", fontFamily: "'Noto Sans', sans-serif", fontSize: '14px', color: "#FFFFFF", '&:hover': {color:"#F2D555"} }} href="#" underline="none">
                                        {'Photography'}
                                    </Link>
                                    <Link sx={{ paddingBottom: "8px", fontFamily: "'Noto Sans', sans-serif", fontSize: '14px', color: "#FFFFFF", '&:hover': {color:"#F2D555"} }} href="#" underline="none">
                                        {'Development'}
                                    </Link>
                                    <Link sx={{ fontFamily: "'Noto Sans', sans-serif", fontSize: '14px', color: "#FFFFFF", '&:hover': {color:"#F2D555"} }} href="#" underline="none">
                                        {'Marketing'}
                                    </Link>
                            </Typography>

                    </Grid>
                    <Grid
                        item
                        xs={3}>
                            <Typography
                                sx={{
                                    fontFamily: "'Noto Sans', sans-serif",
                                    fontWeight: "Bold",
                                    fontSize: '16px',
                                    marginBottom: "14px"
                                }}>Contact Us
                            </Typography>
                            <Typography
                            sx={{
                                fontFamily: "'Noto Sans', sans-serif",
                                fontWeight: "Normal",
                                fontSize: '14px',
                            }}>Indonesia<br />Jl. Planet Namek No. 123, Surabaya<br />Telp : 083849420146<br />Email : Johndoe@seeevent.com
                            </Typography>
                    </Grid>
            </Grid>
            <Grid
                container
                direction="row"
                justifyContent="space-between">
                    <Typography
                        sx={{
                            fontFamily: "'Noto Sans', sans-serif",
                            fontSize: "14px"
                            }}>© 2021 SeeEvent All rights reserved.
                    </Typography>
                    <Grid
                    item
                    direction="row"
                    alignItems="center">
                        <Link sx={{ marginRight: "20px", fontFamily: "'Noto Sans', sans-serif", fontSize: '14px', color: "#FFFFFF", '&:hover': {color:"#F2D555"} }} href="#" underline="none">
                            {'Privacy Policy'}
                        </Link>
                        <Link sx={{ marginRight: "20px", fontFamily: "'Noto Sans', sans-serif", fontSize: '14px', color: "#FFFFFF", '&:hover': {color:"#F2D555"} }} href="#" underline="none">
                            {'Terms of Service'}
                        </Link>
                        <Link sx={{ fontFamily: "'Noto Sans', sans-serif", fontSize: '14px', color: "#FFFFFF", '&:hover': {color:"#F2D555"} }} href="#" underline="none">
                            {'Helps'}
                        </Link>
                </Grid>
            </Grid>
        </Grid>
    )
}

export default Footer;