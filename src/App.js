import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Search from "./Pages/Search/Search";
import Signin from "./Pages/Signin/Signin";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={Signin} />
      </Switch>
    </Router>
  );
}

export default App;
